# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.mediasee.net>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

##############################################################################
# Release Notes
# 2015-02-21 : 1. Init
# 2015-03-05 : 2. Tambahan Property (Pasal, Aturan Dll ) Untuk Jabatan    
#  
##############################################################################
from openerp.osv import fields, osv
from datetime import date,datetime,timedelta
import time
from mx import DateTime
from layanan_jft import _TIPE_LAYANAN_JFT,_TIPE_LAYANAN_PEMBEBASAN_SEMENTARA,_TIPE_LAYANAN_PENGANGKATAN_KEMBALI,_TIPE_LAYANAN_PENGANGKATAN_PERTAMA


#======================== Daftar Nama Jabatan ====================#
class jabatan_fungsional_tertentu(osv.Model):
    _name = 'jabatan.fungsional.tertentu'
    _description ='jabatan fungsional tertentu'
    
    _columns = {
        'name'          : fields.char('Nama Jabatan',size=328,required=True),
        'code'     : fields.char('Kode Jabatan',size=8,required=True),
        'maks_usia'          : fields.integer('Maksimum Usia',required=True),
        'instansi_pembina_id': fields.many2one('instansi.pembina', 'Instansi Pembina Pusat', ),
        'instansi_pembina_daerah_id': fields.many2one('instansi.pembina', 'Instansi Pembina Pusat', ),
        
        'aturan_permenpan'          : fields.text('PERMENPAN',required=True),
        'aturan_perpres'          : fields.text('PERPRES',required=True),
        'aturan_skb'          : fields.text('SKB',required=True),
        
        'pasal_pengangkatan_ids'      : fields.one2many('pasal.pengangkatan.jabatan.fungsional.tertentu','jabatan_id', 'Pengangkatan'),
        'pasal_kenaikan_ids'              : fields.one2many('pasal.kenaikan.jabatan.fungsional.tertentu','jabatan_id', 'Kenaikan Jabatan',),
        'pasal_pembebasan_ids'            : fields.one2many('pasal.pembebasan.jabatan.fungsional.tertentu','jabatan_id', 'Pembebasan'),
        'pasal_pemberhentian_ids'         : fields.one2many('pasal.pemberhentian.jabatan.fungsional.tertentu','jabatan_id', 'Pemberhentian'),
        'pasal_pengangkatan_kembali_ids'  : fields.one2many('pasal.pengangkatan.kembali.jabatan.fungsional.tertentu','jabatan_id', 'Pengangkatan Kembali'),
        'pasal_alih_jenjang_ids'          : fields.one2many('pasal.alih.jenjang.jabatan.fungsional.tertentu','jabatan_id', 'Alih Tingkat'),
        'pasal_perpindahan_ids'          : fields.one2many('pasal.perpindahan.jabatan.fungsional.tertentu','jabatan_id', 'Perpindahan'),
        
        'tunjangan_ids'          : fields.one2many('tunjangan.jabatan.fungsional.tertentu','jabatan_id', 'Tunjangan'),
        
        'formasi_ids': fields.one2many('formasi.jabatan.fungsional.tertentu','jabatan_id', 'Formasi Tersedia'),
        'active'        : fields.boolean('Aktif'),
        
    }
    _defaults = {
        'active' : True,
    }
    _order ='name'

    def _check_unique_name_insesitive(self, cr, uid, ids, context=None):
        sr_ids = self.search(cr, 1 ,[], context=context)
        lst = [
                x.name.lower().strip() for x in self.browse(cr, uid, sr_ids, context=context)
                if x.name and x.id not in ids
              ]
        for self_obj in self.browse(cr, uid, ids, context=context):
            if self_obj.name and self_obj.name.lower() and self_obj.name.strip() and self_obj.name.lower().strip() in  lst:
                return False
        return True

    _constraints = [(_check_unique_name_insesitive, 'Error: Nama Sudah Tersedia ', ['name'])]
    _sql_constraints = [
         ('name_unique', 'unique (name)',
             'Data Tidak Bisa Dimasukan, Data Sudah Tersedia')
     ]
   
jabatan_fungsional_tertentu()
# daftar tunjangan
class tunjangan_jabatan_fungsional_tertentu(osv.Model):
    _name = 'tunjangan.jabatan.fungsional.tertentu'
    _description ='Tunjangan Jabatan Fungsional'
    
    _columns = {
        'tunjangan'          : fields.float('Tunjangan', required=True),
        'jabatan_id': fields.many2one('jabatan.fungsional.tertentu', 'Nama Jabatan', ),
        'jenjang_jabatan_id': fields.many2one('jenjang.jabatan', 'Jenjang Jabatan', required=True),
        
    }
    _order = "tunjangan desc"
tunjangan_jabatan_fungsional_tertentu()

# daftar_pasal
class pasal_pengangkatan_jabatan_fungsional_tertentu(osv.Model):
    _name = 'pasal.pengangkatan.jabatan.fungsional.tertentu'
    _description ='Pasal Pengangkatan Jabatan Fungsional Per Layanan'
    
    _columns = {
        'pasal'          : fields.text('Pasal', required=True),
        'seq'          : fields.integer('Urutan', required=True),
        'jabatan_id': fields.many2one('jabatan.fungsional.tertentu', 'Nama Jabatan', ),
        'tingkat_jabatan_id': fields.many2one('tingkat.jabatan', 'Tingkat Jabatan', required=False),
        'type_id'           : fields.selection(_TIPE_LAYANAN_PENGANGKATAN_PERTAMA,'Jenis Pengangkatan Pertama',required=False),

        
    }
    _order = "seq"
pasal_pengangkatan_jabatan_fungsional_tertentu()
class pasal_kenaikan_jabatan_fungsional_tertentu(osv.Model):
    _name = 'pasal.kenaikan.jabatan.fungsional.tertentu'
    _description ='Pasal Kenaikan Jabatan Fungsional Per Layanan'
    
    _columns = {
        'pasal'          : fields.text('Pasal', required=True),
        'seq'          : fields.integer('Urutan', required=True),
        'jabatan_id': fields.many2one('jabatan.fungsional.tertentu', 'Nama Jabatan', ),
        'tingkat_jabatan_id': fields.many2one('tingkat.jabatan', 'Tingkat Jabatan', required=False),
        
    }
    _order = "seq"
pasal_kenaikan_jabatan_fungsional_tertentu()
class pasal_pembebasan_jabatan_fungsional_tertentu(osv.Model):
    _name = 'pasal.pembebasan.jabatan.fungsional.tertentu'
    _description ='Pasal Pembebasan Jabatan Fungsional Per Layanan'
    
    _columns = {
        'pasal'         : fields.text('Pasal', required=True),
        'remark'         : fields.char('Keterangan', size=128, required=False),
        'seq'           : fields.integer('Urutan', required=True),
        'jabatan_id'    : fields.many2one('jabatan.fungsional.tertentu', 'Nama Jabatan', ),
        'tingkat_jabatan_id': fields.many2one('tingkat.jabatan', 'Tingkat Jabatan', required=False),
        'type'          : fields.selection(_TIPE_LAYANAN_PEMBEBASAN_SEMENTARA,'Tipe Pembebasan Sementara'),
        'golongan_id'   : fields.many2one('partner.employee.golongan', 'Pangkat/Golongan'),
        'other'         : fields.boolean('Other ?')
    }
    _order = "seq"
pasal_pembebasan_jabatan_fungsional_tertentu()
class pasal_pemberhentian_jabatan_fungsional_tertentu(osv.Model):
    _name = 'pasal.pemberhentian.jabatan.fungsional.tertentu'
    _description ='Pasal Pemberhentian Jabatan Fungsional Per Layanan'
    
    _columns = {
        'pasal'        : fields.text('Pasal', required=True),
        'seq'          : fields.integer('Urutan', required=True),
        'jabatan_id'   : fields.many2one('jabatan.fungsional.tertentu', 'Nama Jabatan', ),
        'tingkat_jabatan_id': fields.many2one('tingkat.jabatan', 'Tingkat Jabatan', required=False),

        
    }
    _order = "seq"
pasal_pemberhentian_jabatan_fungsional_tertentu()
class pasal_pengangkatan_kembali_jabatan_fungsional_tertentu(osv.Model):
    _name = 'pasal.pengangkatan.kembali.jabatan.fungsional.tertentu'
    _description ='Pasal Pengangkatan Kembali Jabatan Fungsional Per Layanan'
    
    _columns = {
        'pasal'          : fields.text('Pasal', required=True),
        'seq'          : fields.integer('Urutan', required=True),
        'jabatan_id': fields.many2one('jabatan.fungsional.tertentu', 'Nama Jabatan', ),
        'tingkat_jabatan_id': fields.many2one('tingkat.jabatan', 'Tingkat Jabatan', required=False),
        'type'         : fields.selection(_TIPE_LAYANAN_PENGANGKATAN_KEMBALI,'Tipe Pengangkatan Kembali'),
    }
    _order = "seq"
pasal_pengangkatan_kembali_jabatan_fungsional_tertentu()
class pasal_alih_jenjang_jabatan_fungsional_tertentu(osv.Model):
    _name = 'pasal.alih.jenjang.jabatan.fungsional.tertentu'
    _description ='Pasal Alih Jenjang Jabatan Fungsional Per Layanan'
    
    _columns = {
        'pasal'          : fields.text('Pasal', required=True),
        'seq'          : fields.integer('Urutan', required=True),
        'jabatan_id': fields.many2one('jabatan.fungsional.tertentu', 'Nama Jabatan', ),
        'tingkat_jabatan_id': fields.many2one('tingkat.jabatan', 'Tingkat Jabatan', required=False),
        
        
    }
    _order = "seq"
pasal_alih_jenjang_jabatan_fungsional_tertentu()

class pasal_perpindahan_jabatan_fungsional_tertentu(osv.Model):
    _name = 'pasal.perpindahan.jabatan.fungsional.tertentu'
    _description ='Pasal Perpindahan Jabatan Fungsional Per Layanan'
    
    _columns = {
        'pasal'          : fields.text('Pasal', required=True),
        'seq'          : fields.integer('Urutan', required=True),
        'jabatan_id': fields.many2one('jabatan.fungsional.tertentu', 'Nama Jabatan', ),
        'tingkat_jabatan_id': fields.many2one('tingkat.jabatan', 'Tingkat Jabatan', required=False),
        
    }
    _order = "seq"
pasal_perpindahan_jabatan_fungsional_tertentu()