# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.mediasee.net>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

##############################################################################
# Release Notes
# 2015-02-27 : 1. Init  
#  
##############################################################################
from openerp.osv import fields, osv
from openerp import SUPERUSER_ID
from datetime import date,datetime,timedelta
#import time
from mx import DateTime

_TIPE_LAYANAN_JFT = [('pengangkatan_pertama','Pengangkatan Pertama'),
                     ('kenaikan_jabatan','Kenaikan Jabatan'),
                     ('perpindahan','Perpindahan'),
                     ('pemberhentian','Pemberhentian'),
                     ('alih_Jenjang','Alih Jenjang'),
                     ('pembebasan_sementara','Pembebasan Sementara'),
                     ('pengangkatan_kembali','Pengangkatan Kembali'),
                      ]

                      
_STATE_LAYANAN_JFT = [('draft','Draft'),
                      ('propose_other','Pengajuan Ke OPD Lain'),('propose_other_rejected','Ditolak Dari OPD Tujuan'),
                      ('propose','Pengajuan OPD'),
                      ('propose_rejected','Ditolak Dari OPD'),
                      ('evaluated','Verifikasi BKD'),
                      ('evaluated_rejected','Verifikasi Ditolak'),
                      ('confirm','Diterima'),
                      ('cancel','Dibatalkan'),
                      ]

_TIPE_LAYANAN_PENGANGKATAN_PERTAMA = [('inpassing','Inpassing'),('cpns','CPNS'),('pindah_jabatan','Pindah Jabatan'),
                      ]
_TIPE_LAYANAN_PEMBEBASAN_SEMENTARA =  [('tugas_belajar','Tugas Belajar'),
                                       ('pengangakatan','Diangkat Sebagai Struktural'),
                                       ('hukuman_disiplin','Terkena Hukuman Disiplin'),
                                       ('angka_kredit','Tidak Memenuhi Angka Kredit'),
                                       ('penugasan_lain','Penugasan Lain'),
                                       ('cuti','Cuti Diluar Tanggungan'),
                      ]
_TIPE_LAYANAN_PENGANGKATAN_KEMBALI =  [('tugas_belajar','Tugas Belajar'),
                                       ('pemberhentian_struktural','Mengundurkan  Sebagai Struktural'),
                                       ('hukuman_disiplin','Terkena Hukuman Disiplin'),
                                       ('angka_kredit','Tidak Memenuhi Angka Kredit'),
                                       ('penugasan_lain','Penugasan Lain'),
                                       ('cuti','Cuti Diluar Tanggungan'),
                      ]
_TIPE_VALIDASI = [('usia','Usia'),
                      ]
class validasi_layanan_jft(osv.Model):
    _name = 'validasi.layanan.jft'
    _description ='Validasi Layanan JFT'
    
    _columns = {
        'name'     : fields.char('Nama',size=128,required=True),
        'tipe_validasi_id': fields.selection(_TIPE_VALIDASI,'Jenis Validasi',required=True),
        'tipe_layanan_id': fields.selection(_TIPE_LAYANAN_JFT,'Jenis Layanan',required=True),
        'tipe_layanan_pengangkatan_pertama': fields.selection(_TIPE_LAYANAN_PENGANGKATAN_PERTAMA,'Jenis Pengangkatan Pertama',required=False),
        'tipe_layanan_pengangkatan_kembali': fields.selection(_TIPE_LAYANAN_PENGANGKATAN_KEMBALI,'Jenis Pengangkatan Pertama',required=False),
        'validate_boolean_value':fields.boolean('Validasi Aktif'),
    }

    def release_expire_all_layanan(self, cr, uid, ids, context=None):
        formasi_pool = self.pool.get('formasi.jabatan.fungsional.tertentu')
        pengangkatan_pertama_pool = self.pool.get('layanan.pengangkatan.pertama')
        perpindahan_pool = self.pool.get('layanan.perpindahan')
        alih_tingkat_pool = self.pool.get('layanan.alih.tingkat')
        pengangkatan_kembali_pool = self.pool.get('layanan.pengangkatan.kembali')
        #harus dibuat parameter di konfigurasi
        expire_limit_count = -5;
        
        today = DateTime.today();
        limit_tanggal_pengajuan = today + timedelta(days=expire_limit_count)

        pengangkatan_pertama_ids = pengangkatan_pertama_pool.search(cr,uid,[('state','=','draft'),
                                                                  ('tanggal_pengajuan','<',limit_tanggal_pengajuan),
                                                               ],context=None)
        if pengangkatan_pertama_ids:
          print "Pengangkatan Pertama Expired : ",len(pengangkatan_pertama_ids)
        for layanan_obj in pengangkatan_pertama_pool.browse(cr,uid,pengangkatan_pertama_ids,context=None) :
            if layanan_obj.formasi_id:
                # batalkan layanan
                pengangkatan_pertama_pool.set_cancel(cr,uid,[layanan_obj.id],context=None);
                #penambahan kembali kuota
                mode_kuota='plus'
                code = layanan_obj.tingkat_jabatan_id and layanan_obj.tingkat_jabatan_id.code or ''
                formasi_pool.count_kuota(cr,SUPERUSER_ID,[layanan_obj.formasi_id.id],mode_kuota,code,context=None)
        

        perpindahan_ids = perpindahan_pool.search(cr,uid,[('state','=','draft'),
                                                                  ('tanggal_pengajuan','<',limit_tanggal_pengajuan),
                                                               ],context=None)
        if perpindahan_ids:
          print "Perpindahan Expired : ",len(perpindahan_ids)
        for layanan_obj in perpindahan_pool.browse(cr,uid,perpindahan_ids,context=None) :
            if layanan_obj.formasi_id:
                # batalkan layanan
                perpindahan_pool.set_cancel(cr,uid,[layanan_obj.id],context=None);
                #penambahan kembali kuota
                mode_kuota='plus'
                code = layanan_obj.tingkat_jabatan_id and layanan_obj.tingkat_jabatan_id.code or ''
                formasi_pool.count_kuota(cr,SUPERUSER_ID,[layanan_obj.formasi_id.id],mode_kuota,code,context=None)
        
        alih_tingkat_ids = alih_tingkat_pool.search(cr,uid,[('state','=','draft'),
                                                                  ('tanggal_pengajuan','<',limit_tanggal_pengajuan),
                                                               ],context=None)
        if alih_tingkat_ids:
          print "Alih Tingkat Expired : ",len(alih_tingkat_ids)
        for layanan_obj in alih_tingkat_pool.browse(cr,uid,alih_tingkat_ids,context=None) :
            if layanan_obj.formasi_id:
                # batalkan layanan
                alih_tingkat_pool.set_cancel(cr,uid,[layanan_obj.id],context=None);
                #penambahan kembali kuota
                mode_kuota='plus'
                code = layanan_obj.tingkat_jabatan_id and layanan_obj.tingkat_jabatan_id.code or ''
                formasi_pool.count_kuota(cr,SUPERUSER_ID,[layanan_obj.formasi_id.id],mode_kuota,code,context=None)
                
        pengangkatan_kembali_ids = pengangkatan_kembali_pool.search(cr,uid,[('state','=','draft'),
                                                                  ('tanggal_pengajuan','<',limit_tanggal_pengajuan),
                                                               ],context=None)
        if pengangkatan_kembali_ids:
          print "Pengangkatan Kembali Expired : ",len(pengangkatan_kembali_ids)
        for layanan_obj in pengangkatan_kembali_pool.browse(cr,uid,pengangkatan_kembali_ids,context=None) :
            if layanan_obj.formasi_id:
                # batalkan layanan
                pengangkatan_kembali_pool.set_cancel(cr,uid,[layanan_obj.id],context=None);
                #penambahan kembali kuota
                mode_kuota='plus'
                code = layanan_obj.tingkat_jabatan_id and layanan_obj.tingkat_jabatan_id.code or ''
                formasi_pool.count_kuota(cr,SUPERUSER_ID,[layanan_obj.formasi_id.id],mode_kuota,code,context=None)
                

        return True
validasi_layanan_jft()


#'invoice_date': date.today().strftime('%Y-%m-%d'),