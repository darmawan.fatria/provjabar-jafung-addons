# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.asdarfat.wordpress.com.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv

# ====================== res company ================================= #

class res_partner(osv.Model):
    _inherit = 'res.partner'
    def _get_is_jft(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        for employee in self.browse(cr, uid, ids, context=context):
            if not employee.job_id :
                res[employee.id]=False
            else :
                if employee.job_type == 'jft'  :
                    res[employee.id]=True
                else :
                    res[employee.id]=False
        return res
    _columns = {
        'is_jft' :fields.function(_get_is_jft, method=True, string='JFT', type='boolean', readonly=True,store=False),
        
    }
res_partner()

class partner_employee_job(osv.Model):
    _inherit = "partner.employee.job"
    
    _columns = {
        'jft_id'               : fields.many2one('jabatan.fungsional.tertentu', string='Nama Jabatan'),
        'jenjang_jabatan_id'   : fields.many2one('jenjang.jabatan', 'Jenjang Jabatan'),
     }
partner_employee_job()


