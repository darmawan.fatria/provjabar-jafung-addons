from openerp.osv import fields, osv
from datetime import datetime,timedelta
import time
from mx import DateTime
import openerp.addons.decimal_precision as dp
from openerp.tools.translate import _
from openerp.addons.df_jabatan_fungsional_tertentu.layanan_jft import _TIPE_VALIDASI,_TIPE_LAYANAN_PENGANGKATAN_KEMBALI

from openerp import SUPERUSER_ID
# ====================== Popup Class Object ================================= #

class pengajuan_layanan_pengangkatan_kembali(osv.Model):
    _name = 'pengajuan.layanan.pengangkatan.kembali'
    _description = 'Pengajuan (Pembuatan) Pengangkatan Kembali'
    
    def onchange_formasi_ids(self, cr, uid, ids, jabatan_id, context=None):
        res = {'value': {'formasi_ids': False}}
        if jabatan_id:
            jft_pool = self.pool.get('jabatan.fungsional.tertentu')
            jft_obj = jft_pool.browse(cr, uid, jabatan_id,context=None)
            data_ids = set()
            for formasi in jft_obj.formasi_ids : 
                data_ids.add(formasi.id)
            if data_ids :
                formasi_ids = list(data_ids)        
                res['value']['formasi_ids'] = formasi_ids
        return res
  
    _columns = {
        'user_id'    : fields.many2one('res.users', 'User',required=False),
        'employee_id': fields.many2one('res.partner', 'Pegawai',required=True,domain="[('employee','=',True)]"),
        'jabatan_id'               : fields.many2one('jabatan.fungsional.tertentu', string='Jabatan'),
        'tingkat_jabatan_id'   : fields.many2one('tingkat.jabatan', 'Tingkat Jabatan', ),
        'jenjang_jabatan_id': fields.many2one('jenjang.jabatan', 'Jenjang Jabatan' ),
        'layanan_type_id'           : fields.selection(_TIPE_LAYANAN_PENGANGKATAN_KEMBALI,'Jenis Pengangkatan Kembali',required=True),
        
        'formasi_id'     : fields.many2one('formasi.jabatan.fungsional.tertentu', 'Formasi', readonly=True),
        'company_id'    : fields.many2one('res.company', 'OPD' ,required=False , readonly=True,help='OPD Yang Dituju'),
        'periode_pengajuan' : fields.char('Periode Pengajuan',size=6),
        'tanggal_pengajuan' : fields.date('Tanggal Pengajuan',required=True),
        'notes'             :fields.text('Catatan'),
                #dokumen
        'sk_golongan_id'            : fields.many2one('ir.attachment','SK Pangkat Terakhir',domain="[('user_id','=',user_id)]", ),
        'file_sertifikat_diklat'    : fields.many2one('ir.attachment','Sertifikat Diklat',domain="[('user_id','=',user_id)]"),
        'kepgub_jabatan_terakhir'       : fields.many2one('ir.attachment','Keputusan Gubernur Jabatan Terakhir',domain="[('user_id','=',user_id)]"),
        'kepgub_pembebasan_sementara'   : fields.many2one('ir.attachment','Keputusan Gubernur Pembebasan Sementara',domain="[('user_id','=',user_id)]"),
        'kepgub_penempatan_kembali'     : fields.many2one('ir.attachment','Keputusan Gubernur Penempatan Kembali',domain="[('user_id','=',user_id)]"),
        'kepgub_pemberhentian_stuktural'     : fields.many2one('ir.attachment','Keputusan Gubernur Pemberhentian Dari Jab Struktural',domain="[('user_id','=',user_id)]"),
        'kepgub_hukuman_disiplin'       : fields.many2one('ir.attachment','Keputusan Gubernur Hukuman Disiplin',domain="[('user_id','=',user_id)]"),
        'kepgub_cuti_ltn'               : fields.many2one('ir.attachment','Keputusan Gubernur Cuti LTN',domain="[('user_id','=',user_id)]"),

         'nilai_skp'                 : fields.float('Nilai SKP' ,digits_compute=dp.get_precision('normal')),
        'nilai_pak'                 : fields.float('Nilai PAK' ,digits_compute=dp.get_precision('angka_kredit')),
        'file_skp'                  : fields.many2one('ir.attachment','Lampiran SKP',domain="[('user_id','=',user_id)]"),
        'file_pak'                  : fields.many2one('ir.attachment','Lampiran PAK',domain="[('user_id','=',user_id)]"),
       
    }
    _defaults = {
        'periode_pengajuan': lambda *args: time.strftime('%Y%m'),
        'tanggal_pengajuan': lambda *args: time.strftime('%Y-%m-%d'),
      
    }
   
            
    def action_create_pengangkatan_kembali(self, cr, uid, ids, context=None):
        """  Pengangkatan Kembali
        """
        
        vals = {}
        layanan_pool = self.pool.get('layanan.pengangkatan.kembali')
        validator_pool = self.pool.get('validasi.layanan.jft')
        jenjang_jabatan_pool = self.pool.get('jenjang.jabatan')
        #jenjang_by_golongan_pool = self.pool.get('golongan.jabatan.fungsional.tertentu')
        state = 'draft'
        if context and context.get('state',False):
            state = context.get('state','draft')
        for pengajuan_obj in self.browse(cr, uid, ids, context=context) :
            vals = {}
            tingkat_jabatan_id=jenjang_jabatan_id=formasi_id=user_id_opd_other=company_id=False
            if pengajuan_obj.formasi_id:
                formasi_id = pengajuan_obj.formasi_id.id
                company_id                  = pengajuan_obj.employee_id.company_id.id
                user_id_opd_other           = pengajuan_obj.employee_id.company_id.user_id_opd_jafung.id
                jabatan_id                  = pengajuan_obj.formasi_id.jabatan_id.id
                tingkat_jabatan_id          = pengajuan_obj.tingkat_jabatan_id.id
                
                jenjang_jabatan_ids = jenjang_jabatan_pool.search(cr,uid,[('tingkat_jabatan_id','=',tingkat_jabatan_id),
                                                               ],context=None)
            
                for jenjang_jabatan_obj in jenjang_jabatan_pool.browse(cr,uid,jenjang_jabatan_ids,context=None):
                    for golongan_minimum_obj in jenjang_jabatan_obj.golongan_ids :
                        if pengajuan_obj.employee_id.golongan_id \
                            and  golongan_minimum_obj.golongan_id.id  ==  pengajuan_obj.employee_id.golongan_id.id :
                            jenjang_jabatan_id = jenjang_jabatan_obj.id
            
            user_company_id      = pengajuan_obj.employee_id.company_id.id
            user_id_opd_employee = pengajuan_obj.employee_id.company_id.user_id_opd_jafung.id
            user_id_bkd          = pengajuan_obj.employee_id.company_id.user_id_bkd_jafung.id
            employee             = pengajuan_obj.employee_id
            if employee.is_jft and not formasi_id:
                jabatan_id           = employee.job_id.jft_id.id
                jenjang_jabatan_id   = employee.job_id.jenjang_jabatan_id.id
                tingkat_jabatan_id   = employee.job_id.jenjang_jabatan_id.tingkat_jabatan_id.id
                
            
            #validator
            # TO DO:
            #Validator Tingkat Jabatan And Dont Validate This
            
            #Validasi USIA
            validator_usia_ids = validator_pool.search(cr,uid,[('tipe_validasi_id','=','usia'),
                                                               ('tipe_layanan_id','=','pengangkatan_kembali'),
                                                               ('tipe_layanan_pengangkatan_kembali','=',pengajuan_obj.layanan_type_id),
                                                               ],context=None)
            if validator_usia_ids :
                for validator_obj in validator_pool.browse(cr,uid,validator_usia_ids, context=None):
                    if validator_obj.validate_boolean_value:
                        maks_usia =  employee.job_id.jft_id.maks_usia
                        if  employee.usia >  maks_usia:
                            raise osv.except_osv(_('Maaf Persyaratan Usia Tidak Mencukupi'),
                                _('Batas Maksimum Usia Untuk Jabatan Ini Adalah %s ')% maks_usia)
                            
            vals.update({    #Data Layanan
                            'name'                  : 'Pengangkatan Kembali '+employee.name,
                            'jabatan_id'            : jabatan_id,
                            'layanan_type_id'       : pengajuan_obj.layanan_type_id,
                            'tingkat_jabatan_id'    : tingkat_jabatan_id,
                            'jenjang_jabatan_id'    : jenjang_jabatan_id,
                            'periode_pengajuan'     : pengajuan_obj.periode_pengajuan,
                            'tanggal_pengajuan'     : pengajuan_obj.tanggal_pengajuan,
                            'notes'                 : pengajuan_obj.notes,
                            'state'                 : 'draft',
                            'formasi_id'            : formasi_id,
                            #persyaratan
                            'sk_golongan_id'              : pengajuan_obj.sk_golongan_id and pengajuan_obj.sk_golongan_id.id or None,
                            'file_sertifikat_diklat'      : pengajuan_obj.file_sertifikat_diklat and pengajuan_obj.file_sertifikat_diklat.id or None,
                            'kepgub_jabatan_terakhir'     : pengajuan_obj.kepgub_jabatan_terakhir and pengajuan_obj.kepgub_jabatan_terakhir.id or None,

                            'kepgub_pembebasan_sementara'            : pengajuan_obj.kepgub_pembebasan_sementara and pengajuan_obj.kepgub_pembebasan_sementara.id or None,
                            'kepgub_penempatan_kembali'      : pengajuan_obj.kepgub_penempatan_kembali and pengajuan_obj.kepgub_penempatan_kembali.id or None,
                            'kepgub_pemberhentian_stuktural'     : pengajuan_obj.kepgub_pemberhentian_stuktural and pengajuan_obj.kepgub_pemberhentian_stuktural.id or None,
                            'kepgub_hukuman_disiplin'            : pengajuan_obj.kepgub_hukuman_disiplin and pengajuan_obj.kepgub_hukuman_disiplin.id or None,
                            'kepgub_cuti_ltn'      : pengajuan_obj.kepgub_cuti_ltn and pengajuan_obj.kepgub_cuti_ltn.id or None,
                            'file_skp'     : pengajuan_obj.file_skp and pengajuan_obj.file_skp.id or None,
                            'nilai_skp'     : pengajuan_obj.nilai_skp,
                            'file_pak'     : pengajuan_obj.file_pak and pengajuan_obj.file_pak.id or None,
                            'nilai_pak'     : pengajuan_obj.nilai_pak,
                            
                            #Data Pegawai
                            'user_id'             : pengajuan_obj.employee_id.user_id.id,
                            'user_company_id'     : user_company_id,
                            'company_id'          : company_id or user_company_id,
                            'employee_id'         : employee.id,
                            'user_id_opd_employee':user_id_opd_employee,
                            'user_id_opd_other'   : user_id_opd_other or user_id_opd_employee,
                            'user_id_bkd'         :user_id_bkd,
                            #Default
                            'active':True,
                        })
            new_layanan_id=layanan_pool.create(cr, uid,  vals, context)
            if state=='propose':
                layanan_pool.action_propose(cr,uid,[new_layanan_id],context=None)
            
        return True;
        
    
pengajuan_layanan_pengangkatan_kembali()
