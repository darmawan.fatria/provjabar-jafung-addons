from openerp.osv import fields, osv
from datetime import datetime,timedelta
import time
from mx import DateTime
import openerp.addons.decimal_precision as dp
from openerp.tools.translate import _
from openerp.addons.df_jabatan_fungsional_tertentu.layanan_jft import _TIPE_VALIDASI
from openerp import SUPERUSER_ID
# ====================== Popup Class Object ================================= #

class pengajuan_layanan_kenaikan_jabatan(osv.Model):
    _name = 'pengajuan.layanan.kenaikan.jabatan'
    _description = 'Pengajuan (Pembuatan) Kenaikan Jabatan'
    
    
  
    _columns = {
        'user_id': fields.many2one('res.users', 'User',required=True),
        'employee_id': fields.related('user_id', 'partner_id',   type="many2one", relation='res.partner', string='Pegawai'),
        'nip'     : fields.char('NIP',size=20),    
        'golongan_id': fields.many2one('partner.employee.golongan', 'Pangkat/Gol/Ruang',readonly=True),
        'department_id': fields.many2one('partner.employee.department', 'Unit Kerja',readonly=True),
        'job_id': fields.many2one('partner.employee.job', 'Jabatan',readonly=True),
        
        'tingkat_jabatan_id'   : fields.many2one('tingkat.jabatan', 'Tingkat Jabatan', ),
        'jenjang_jabatan_id': fields.many2one('jenjang.jabatan', 'Jenjang Jabatan' ),
        
        'periode_pengajuan' : fields.char('Periode Pengajuan',size=6),
        'tanggal_pengajuan' : fields.date('Tanggal Pengajuan',required=True),
        'notes'             :fields.text('Catatan'),
         #dokumen
        'sk_golongan_id'            : fields.many2one('ir.attachment','SK Pangkat Terakhir',domain="[('user_id','=',user_id)]", ),
        'ijazah_pendidikan_id'      : fields.many2one('ir.attachment','Ijazah',domain="[('user_id','=',user_id)]",), 
        'file_sertifikat_diklat'    : fields.many2one('ir.attachment','Sertifikat Diklat',domain="[('user_id','=',user_id)]"),
        'kepgub_jabatan_terakhir'     : fields.many2one('ir.attachment','Keputusan Gubernur Jabatan Terakhir',domain="[('user_id','=',user_id)]"),
        'nilai_skp'                 : fields.float('Nilai SKP' ,digits_compute=dp.get_precision('normal')),
        'nilai_pak'                 : fields.float('Nilai PAK' ,digits_compute=dp.get_precision('angka_kredit')),
        'file_skp' : fields.many2one('ir.attachment','File SKP',domain="[('user_id','=',user_id)]"),
        'file_pak'     : fields.many2one('ir.attachment','File PAK',domain="[('user_id','=',user_id)]"),
    }
    
   
            
    def action_create_kenaikan_jabatan(self, cr, uid, ids, context=None):
        """  Pengangkatan Pertama
        """
        
        vals = {}
        layanan_pool = self.pool.get('layanan.kenaikan.jabatan')
        validator_pool = self.pool.get('validasi.layanan.jft')
        jenjang_jabatan_pool = self.pool.get('jenjang.jabatan')
        #jenjang_by_golongan_pool = self.pool.get('golongan.jabatan.fungsional.tertentu')
        state = 'draft'
        if context and context.get('state',False):
            state = context.get('state','draft')
        for pengajuan_obj in self.browse(cr, uid, ids, context=context) :
            vals = {}
            user_company_id      = pengajuan_obj.user_id.company_id.id
            user_id_opd_employee = pengajuan_obj.user_id.company_id.user_id_opd_jafung.id
            user_id_bkd          = pengajuan_obj.user_id.company_id.user_id_bkd_jafung.id
            
            
            tingkat_jabatan_id = pengajuan_obj.tingkat_jabatan_id.id
            # jenjang by type
            jenjang_jabatan_id = pengajuan_obj.jenjang_jabatan_id.id
            jabatan_id          = pengajuan_obj.user_id.partner_id and pengajuan_obj.user_id.partner_id.job_id and pengajuan_obj.user_id.partner_id.job_id.jft_id and pengajuan_obj.user_id.partner_id.job_id.jft_id.id
            #validator
            # TO DO:
            #Validator Tingkat Jabatan And Dont Validate This
            
            #Validasi USIA
            validator_usia_ids = validator_pool.search(cr,uid,[('tipe_validasi_id','=','usia'),
                                                               ('tipe_layanan_id','=','kenaikan_jabatan'),
                                                               ],context=None)
            if validator_usia_ids :
                for validator_obj in validator_pool.browse(cr,uid,validator_usia_ids, context=None):
                    if validator_obj.validate_boolean_value:
                        maks_usia =  pengajuan_obj.user_id.partner_id.jabatan_id.maks_usia
                        if  pengajuan_obj.user_id.partner_id.usia >  maks_usia:
                            raise osv.except_osv(_('Maaf Persyaratan Usia Tidak Mencukupi'),
                                _('Batas Maksimum Usia Untuk Jabatan Ini Adalah %s ')% maks_usia)
                            
            vals.update({    #Data Layanan
                            'name'                  : 'Kenaikan Jabatan '+pengajuan_obj.jenjang_jabatan_id.name,
                            'tingkat_jabatan_id'    : tingkat_jabatan_id,
                            'periode_pengajuan'     : pengajuan_obj.periode_pengajuan,
                            'tanggal_pengajuan'     : pengajuan_obj.tanggal_pengajuan,
                            'notes'                 : pengajuan_obj.notes,
                            'jenjang_jabatan_id'    : jenjang_jabatan_id,
                            'jabatan_id'    : jabatan_id,
                            'state'                 : 'draft',
                            #persyaratan
                            'nilai_pak':pengajuan_obj.nilai_pak,
                            'nilai_skp':pengajuan_obj.nilai_skp,
                            'file_skp'            : pengajuan_obj.file_skp and pengajuan_obj.file_skp.id or None,
                            'file_pak'      : pengajuan_obj.file_pak and pengajuan_obj.file_pak.id or None,
                            'sk_golongan_id'            : pengajuan_obj.sk_golongan_id and pengajuan_obj.sk_golongan_id.id or None,
                            'ijazah_pendidikan_id'      : pengajuan_obj.ijazah_pendidikan_id and pengajuan_obj.ijazah_pendidikan_id.id or None,
                            'file_sertifikat_diklat'    : pengajuan_obj.file_sertifikat_diklat and pengajuan_obj.file_sertifikat_diklat.id or None,
                            'kepgub_jabatan_terakhir'     : pengajuan_obj.kepgub_jabatan_terakhir and pengajuan_obj.kepgub_jabatan_terakhir.id or None,
                            
                            #Data Pegawai
                            'user_id'             : pengajuan_obj.user_id.id,
                            'user_company_id'     : user_company_id,
                            'company_id'            : user_company_id,
                            'employee_id'         : pengajuan_obj.user_id.partner_id and  pengajuan_obj.user_id.partner_id.id,
                            'user_id_opd_employee':user_id_opd_employee,
                            'user_id_bkd'           :user_id_bkd,
                            #Default
                            'active':True,
                        })
            new_layanan_id=layanan_pool.create(cr, uid,  vals, context)
            if state=='propose':
                layanan_pool.action_propose(cr,uid,[new_layanan_id],context=None)
            
        return True;
        
    
pengajuan_layanan_kenaikan_jabatan()
