##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.mediasee.net>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

{
    "name": "Sistem Informasi Jabatan Fungsional Tertentu",
    "version": "1.0",
    "author": "Darmawan Fatriananda",
    "category": "Partner/Kepegawaian",
    "description": """
        v.01 : 
        - Formasi Jabatan Fungsional Tertentu  | 21 Februari 2015
        - Layanan JFT  | 27 Februari 2015
        - Layanan Perpindahan | 25 Maret 2015
        - Layanan Alih Tingkat | 25 Maret 2015
        - Layanan Kenaikan Jabatan | 26 Maret 2015
        - Layanan Pemberhentian | 27 Maret 2015
        - Layanan Pembebasan Sementara | 27 Maret 2015
        - Layanan Pengangkatan Kembali | 28 Maret 2015
        - Pendaftaran Layanan Pembehentian | 1 April 2015
        - Pendaftaran Layanan Pengangkatan Kembali | 1 April 2015
    """,
    "website" : "http://www.mediasee.net",
    "license" : "GPL-3",
    "depends": [
                "mail",
                "df_partner_employee",
                ],
    'data': [       "security/jabatan_fungsional_tertentu_security.xml",
                    "formasi_jabatan_fungsional_tertentu_view.xml",
                    "jabatan_fungsional_tertentu_view.xml",
                    "formasi_jabatan_fungsional_tertentu_config_view.xml",
                    
                    "layanan_pengangkatan_pertama_view.xml",
                    "popup_view/pengajuan_layanan_pengangkatan_pertama_view.xml",
                    "layanan_perpindahan_view.xml",
                    "popup_view/pengajuan_layanan_perpindahan_view.xml",
                    "layanan_alih_tingkat_view.xml",
                    "popup_view/pengajuan_layanan_alih_tingkat_view.xml",
                    "layanan_kenaikan_jabatan_view.xml",
                    "popup_view/pengajuan_layanan_kenaikan_jabatan_view.xml",
                    "layanan_pemberhentian_view.xml",
                    "popup_view/pengajuan_layanan_pemberhentian_view.xml",
                    "layanan_pembebasan_sementara_view.xml",
                    "popup_view/pengajuan_layanan_pembebasan_sementara_view.xml",
                    "layanan_pengangkatan_kembali_view.xml",
                    "popup_view/pengajuan_layanan_pengangkatan_kembali_view.xml",
                    
                    "layanan_jft_view.xml",
                    
                    "company_view.xml",
                    "partner_employee_view.xml",
                   ],
    #'demo_xml': [],
    'installable': True,
    #'active': False,
}
