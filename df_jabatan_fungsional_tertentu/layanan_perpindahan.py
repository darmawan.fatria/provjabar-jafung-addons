# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.mediasee.net>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

##############################################################################
# Release Notes
# 2015-03-23 : 1. Init  
#  
##############################################################################
from openerp.osv import fields, osv
from datetime import date,datetime,timedelta
import time
from openerp.tools.translate import _
from mx import DateTime
from layanan_jft import _STATE_LAYANAN_JFT
import openerp.addons.decimal_precision as dp

#======================== Layanan Jafung Perpindahan ====================#
class layanan_perpindahan(osv.Model):
    _name = 'layanan.perpindahan'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description ='Layanan Perpindahan Jafung'
    
    def _tunjangan_by_jenjang_jabatan(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        
        jumlah_tunjangan=0.0
        for data in self.browse(cr,uid,ids,context=None):
            if data.jenjang_jabatan_id and data.jabatan_id and data.jabatan_id.tunjangan_ids :
                for list_obj in data.jabatan_id.tunjangan_ids:
                    if list_obj.jenjang_jabatan_id.id == data.jenjang_jabatan_id.id:
                        jumlah_tunjangan = list_obj.tunjangan
            res[data.id]=jumlah_tunjangan
        return res
    
    _columns = {
        'name'                      : fields.char('Layanan Jabatan',size=150),
        'formasi_id'                : fields.many2one('formasi.jabatan.fungsional.tertentu', 'Formasi' ),
        'jabatan_id'                : fields.related('formasi_id', 'jabatan_id',   type="many2one", relation='jabatan.fungsional.tertentu', string='Nama Jabatan', store=False),
        'tingkat_jabatan_id'   : fields.many2one('tingkat.jabatan', 'Tingkat Jabatan',required=True ),
        'jenjang_jabatan_id'   : fields.many2one('jenjang.jabatan', 'Jenjang Jabatan'),
        'state'                     : fields.selection(_STATE_LAYANAN_JFT,'Status',required=True),
        
        'user_id'                   : fields.many2one('res.users', 'User ID' ,required=True ),
        'employee_id'               : fields.related('user_id', 'partner_id',   type="many2one", relation='res.partner', string='Pegawai', store=True),
        'nip'                       : fields.related('employee_id', 'nip',   type="char", string='NIP'),
        'golongan_id'               : fields.related('employee_id', 'golongan_id',   type="many2one", relation='partner.employee.golongan', string='Pangkat/Gol.Ruang'),
        'department_id'             : fields.related('employee_id', 'department_id',   type="many2one", relation='partner.employee.department', string='Unit Kerja'),
        'job_id'                    : fields.related('employee_id', 'job_id',   type="many2one", relation='partner.employee.job', string='Jabatan'),
        
        'company_id'                : fields.many2one('res.company', 'OPD Formasi' ,required=True ),
        'user_company_id'           : fields.many2one('res.company', 'OPD Pegawai' ,required=True ),
        
        'user_id'                   : fields.many2one('res.users', 'User Login',readonly=True  ,required=True ),
        'user_id_opd_employee'      : fields.many2one('res.users', 'Verifikatur OPD Pegawai' ,readonly=True ,required=True ),
        'user_id_opd_other'         : fields.many2one('res.users', 'Verifikatur OPD Formasi' ,readonly=True ,required=True ),
        'user_id_bkd'               : fields.many2one('res.users', 'Verifikatur BKD' ,readonly=True ,required=True ),
        
        'periode_pengajuan'         : fields.char('Periode Pengajuan',size=6),
        'tanggal_pengajuan'         : fields.date('Tanggal Pengajuan',required=True),
        #surat pengantar
        'no_surat_pengantar'        : fields.char('Nomor Surat Pengantar',size=50),
        'perihal_surat_pengantar'        : fields.char('Perihal Surat Pengantar',size=150),
        'tanggal_surat_pengantar'         : fields.date('Tanggal Surat Pengantar'),
        'file_surat_pengantar'         : fields.binary('Surat Pengantar'),
        #surat keputusan gubernur
        'no_surat_keputusan'        : fields.char('No Surat Keputusan',size=50),
        'perihal_surat_keputusan'        : fields.char('Perihal Surat Keputusan',size=150),
        'tanggal_surat_keputusan'         : fields.date('Tanggal Surat Keputusan'),
        'file_surat_keputusan'         : fields.binary('Surat Keputusan'),
        #tunjangan jabatan
        'tunjangan_jabatan': fields.function(_tunjangan_by_jenjang_jabatan, method=True,readonly=True , store=False,
                                             type="float",string='Tunjangan'),
        #dokumen
        'sk_golongan_id'            : fields.many2one('ir.attachment','SK Pangkat Terakhir',domain="[('user_id','=',user_id)]", ),
        'kepgub_jabatan_terakhir'     : fields.many2one('ir.attachment','Keputusan Gubernur Jabatan Terakhir',domain="[('user_id','=',user_id)]"),
        #lainnya
        'notes'                     :fields.text('Catatan'),
        'notes_rejected'            :fields.text('Catatan Penolakan'),
        'nilai_skp'                 : fields.float('Nilai SKP' ,digits_compute=dp.get_precision('normal')),
        'nilai_pak'                 : fields.float('Nilai PAK' ,digits_compute=dp.get_precision('angka_kredit')),
        'file_skp'                  : fields.many2one('ir.attachment','File SKP',domain="[('user_id','=',user_id)]"),
        'file_pak'                  : fields.many2one('ir.attachment','File PAK',domain="[('user_id','=',user_id)]"),

       
        'active'                    : fields.boolean('Aktif'),
        
    }
    _defaults = {
        'active' : True,
    }
    _order ='tanggal_pengajuan desc'
   
    # WORKFLOW    
    def set_draft(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'draft'}, context=context) 
    def set_propose(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'propose'}, context=context)
    def set_propose_other(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'propose_other'}, context=context)
    def set_propose_rejected(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'propose_rejected'}, context=context)
    def set_propose_other_rejected(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'propose_other_rejected'}, context=context)
    def set_evaluated(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'evaluated'}, context=context)
    def set_evaluated_rejected(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'evaluated_rejected'}, context=context)
    def set_confirm(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'confirm'}, context=context)
    def set_cancel(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'cancel','active':False}, context=context)
    
    def action_draft(self, cr, uid, ids, context=None):
        for obj in self.browse(cr,uid,ids,context):
            self.set_draft(cr, uid, [obj.id], context)
        return True
    def action_propose(self, cr, uid, ids, context=None):
        for obj in self.browse(cr,uid,ids,context):
            if self.in_same_company_id(obj.company_id, obj.user_company_id):
                self.set_propose(cr, uid, [obj.id], context)
            else :
                self.set_propose_other(cr, uid, [obj.id], context)
        return True 
    def action_propose_rejected(self, cr, uid, ids, context=None):
        for obj in self.browse(cr,uid,ids,context):
            if self.in_same_company_id(obj.company_id, obj.user_company_id):
                self.set_propose_rejected(cr, uid, [obj.id], context)
            else :
                self.set_propose_other_rejected(cr, uid, [obj.id], context)
        return True
    def action_evaluated(self, cr, uid, ids, context=None):
        for obj in self.browse(cr,uid,ids,context):
            if self.validate_surat_pengantar(obj) :
                self.set_evaluated(cr, uid, [obj.id], context)
        return True
    def action_evaluated_rejected(self, cr, uid, ids, context=None):
        for obj in self.browse(cr,uid,ids,context):
            self.set_evaluated_rejected(cr, uid, [obj.id], context)
        return True
    
    def action_confirm(self, cr, uid, ids, context=None):
        for obj in self.browse(cr,uid,ids,context):
            self.set_confirm(cr, uid, [obj.id], context)
        return True
    
    
    
    #validasi
    def in_same_company_id(self,formasi_company_id,user_company_id):
        if formasi_company_id and  user_company_id:
            if formasi_company_id.id == user_company_id.id:
                return True
            else :
                return False
        else :
            raise osv.except_osv(_('Error'),
                                        _('Data OPD tidak lengkap.'))
    def validate_surat_pengantar(self,an_obj):
        if an_obj.no_surat_pengantar and an_obj.tanggal_surat_pengantar and an_obj.perihal_surat_pengantar:
            return True;
        raise osv.except_osv(_('Error'),
                                        _('Silahkan lengkapi data Surat Pengantar Dari OPD.'))    
layanan_perpindahan()

class formasi_jabatan_fungsional_tertentu(osv.Model):
    _inherit = 'formasi.jabatan.fungsional.tertentu'
    
    def action_create_perpindahan_ahli_popup(self, cr, uid, ids, context=None):
        """ Pengajuan Perpindahan Ahli
        """
        
        if not ids: return []
        dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'df_jabatan_fungsional_tertentu', 'action_create_perpindahan_ahli_terampil_form_view')
        formasi_obj = self.browse(cr, uid, ids[0], context=context)
        user_obj = self.pool.get('res.users').browse(cr,uid,uid,context=context)
        tingkat_jabatan_id = self.pool.get('tingkat.jabatan').search(cr,uid,[('code','=','ahli')],context=None)
        today = date.today()
        #print " jenjang_jabatan_id: ",jenjang_jabatan_id
        if not user_obj.partner_id :
             raise osv.except_osv(_('Proses Pengajuan Tidak Bisa Dilanjutkan'),
                                _('Silahkan Lengkapi Data Kepegawaian Anda, Atau Hubungi Admin'))
        #atribut kepegawaian
        nip = user_obj.partner_id.nip or ''
        golongan_id = user_obj.partner_id.golongan_id and user_obj.partner_id.golongan_id.id or ''
        department_id = user_obj.partner_id.department_id and user_obj.partner_id.department_id.id or ''
        job_id  = user_obj.partner_id.job_id and user_obj.partner_id.job_id.id or ''
        #cek presyaratan
        
        
        return {
                'name':_("Perpindahan Jabatan Fungsional Ahli"),
                'view_mode': 'form',
                'view_id': view_id,
                'view_type': 'form',
                'res_model': 'pengajuan.layanan.perpindahan',
                'type': 'ir.actions.act_window',
                'nodestroy': True,
                'target': 'new',
                'domain': '[]',
                'context': {
                    'default_formasi_id':formasi_obj.id,
                    'default_company_id':formasi_obj.company_id.id,
                    'default_user_id':uid,
                    'default_tanggal_pengajuan':today.strftime('%Y-%m-%d'),
                    'default_periode_pengajuan':today.strftime('%Y%m'),
                    'default_tingkat_jabatan_id':tingkat_jabatan_id and tingkat_jabatan_id[0] or None,
                    'default_nip':nip,
                    'default_golongan_id':golongan_id,
                    'default_department_id':department_id,
                    'default_job_id':job_id,
                }
            }
    def action_create_perpindahan_terampil_popup(self, cr, uid, ids, context=None):
        """ Pengajuan Perpindahan Ahli
        """
        
        if not ids: return []
        dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'df_jabatan_fungsional_tertentu', 'action_create_perpindahan_ahli_terampil_form_view')
        formasi_obj = self.browse(cr, uid, ids[0], context=context)
        user_obj = self.pool.get('res.users').browse(cr,uid,uid,context=context)
        tingkat_jabatan_id = self.pool.get('tingkat.jabatan').search(cr,uid,[('code','=','terampil')],context=None)
        today = date.today()
        if not user_obj.partner_id :
             raise osv.except_osv(_('Proses Pengajuan Tidak Bisa Dilanjutkan'),
                                _('Silahkan Lengkapi Data Kepegawaian Anda, Atau Hubungi Admin'))
        #atribut kepegawaian
        nip = user_obj.partner_id.nip or ''
        golongan_id = user_obj.partner_id.golongan_id and user_obj.partner_id.golongan_id.id or ''
        department_id = user_obj.partner_id.department_id and user_obj.partner_id.department_id.id or ''
        job_id  = user_obj.partner_id.job_id and user_obj.partner_id.job_id.id or ''
        #cek presyaratan
        
        
        return {
                'name':_("Perpindahan Jabatan Fungsional Terampil"),
                'view_mode': 'form',
                'view_id': view_id,
                'view_type': 'form',
                'res_model': 'pengajuan.layanan.perpindahan',
                'type': 'ir.actions.act_window',
                'nodestroy': True,
                'target': 'new',
                'domain': '[]',
                'context': {
                    'default_formasi_id':formasi_obj.id,
                    'default_company_id':formasi_obj.company_id.id,
                    'default_user_id':uid,
                    'default_tanggal_pengajuan':today.strftime('%Y-%m-%d'),
                    'default_periode_pengajuan':today.strftime('%Y%m'),
                    'default_tingkat_jabatan_id':tingkat_jabatan_id and tingkat_jabatan_id[0] or None,
                    'default_nip':nip,
                    'default_golongan_id':golongan_id,
                    'default_department_id':department_id,
                    'default_job_id':job_id,
                }
            }
formasi_jabatan_fungsional_tertentu()    