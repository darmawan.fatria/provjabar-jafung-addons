# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.mediasee.net>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

##############################################################################
# Release Notes
# 2015-02-21 : 1. Init  
#  
##############################################################################
from openerp.osv import fields, osv
from datetime import date,datetime,timedelta
import time
from mx import DateTime


# ====================== res partner ================================= #

class instansi_pembina(osv.Model):
    _name = 'instansi.pembina'
    _description ='Instansi Pembina'
    
    _columns = {
        'name'     : fields.char('Nama Instansi Pembina',size=128,required=True),
        'code'     : fields.char('Kode Instansi Pembina',size=8),
        'tembusan' : fields.text('Kalimat Tembusan'),
    }
   

instansi_pembina()
class tingkat_jabatan(osv.Model):
    _name = 'tingkat.jabatan'
    _description ='Tingkat Jabatan'
    
    _columns = {
        'name'     : fields.char('Nama Tingkat Jabatan',size=25,required=True),
        'code'     : fields.char('Kode Tingkat Jabatan',size=8),
    }
   

tingkat_jabatan()
class jenjang_jabatan(osv.Model):
    _name = 'jenjang.jabatan'
    _description ='Level Jenjang Jabatan'
    
    _columns = {
        'name'     : fields.char('Nama Level Jenjang Jabatan',size=50,required=True),
        'code'     : fields.char('Kode Level Jenjang Jabatan',size=20),
        'level'     : fields.integer('Kelas Jabatan',required=True),
        'tingkat_jabatan_id': fields.many2one('tingkat.jabatan', 'Tingkat Jabatan',required=True ),
        'employee_id': fields.many2one('res.partner', 'Penanda Tangan',required=True ),
        'golongan_ids': fields.one2many('golongan.jabatan.fungsional.tertentu','jenjang_jabatan_id', 'Minimum Golongan'),
    }
    _order = "level"

jenjang_jabatan()
# detail list
class golongan_jabatan_fungsional_tertentu(osv.Model):
    _name = 'golongan.jabatan.fungsional.tertentu'
    _description ='golongan jabatan fungsional tertentu'
    
    _columns = {
        'name'          : fields.char('Nama',size=50),
        'jenjang_jabatan_id': fields.many2one('jenjang.jabatan', 'Jenjang Jabatan', required=True),
        'golongan_id': fields.many2one('partner.employee.golongan', 'Golongan', required=True),
    }
golongan_jabatan_fungsional_tertentu()

class global_parameter_jft(osv.Model):
    _name = 'global.parameter.jft'
    _description ='Global Parameter'
    
    _columns = {
        'name'     : fields.char('Nama',size=128,required=True),
        'code'     : fields.char('Kode',size=12,required=True),
        'value'     : fields.text('Nilai',required=True),
    }
    _sql_constraints = [
         ('code_unique', 'unique (code)',
             'Duplikat Kode')
     ]
    def get_value_from_key(self,cr,uid,key,context=None):
        data_ids =self.search(cr,uid,[('code','=',key)],context=None)
        if data_ids and len(data_ids) ==1:
            data = self.browse(cr,uid,data_ids[0],context=None)
            return data.value
        return ''
global_parameter_jft()