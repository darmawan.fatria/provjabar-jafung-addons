# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.mediasee.net>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

##############################################################################
# Release Notes
# 2015-02-21 : 1. Init
# 2015-03-05 : 2. Tambahan Property (Pasal, Aturan Dll ) Untuk Jabatan    
#  
##############################################################################
from openerp.osv import fields, osv
from datetime import date,datetime,timedelta
import time
from mx import DateTime
from openerp import SUPERUSER_ID
# ====================== Formasi ================================= #
class formasi_jabatan_fungsional_tertentu(osv.Model):
    _name = 'formasi.jabatan.fungsional.tertentu'
    _description ='formasi jabatan fungsional tertentu'
    
    def create(self, cr, uid, vals, context=None):
        
        if not vals.get('name',False) :
            jabatan_id = vals.get('jabatan_id',False)
            if jabatan_id :
                jabatan_obj =  self.pool.get('jabatan.fungsional.tertentu').browse(cr,uid,jabatan_id,context=None)
                vals.update({'name':jabatan_obj.name})
        return super(formasi_jabatan_fungsional_tertentu, self).create(cr, uid, vals, context)
    def _get_kuota_ahli(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        kuota=0
        today = date.today()
        for record in self.read(cr,uid,ids,["id","kuota_ahli_awal","kuota_ahli_terdaftar"],context):
                kuota = record['kuota_ahli_awal'] - record['kuota_ahli_terdaftar']
                res[record['id']]=kuota
        return res
    def _get_kuota_terampil(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        kuota=0
        today = date.today()
        for record in self.read(cr,uid,ids,["id","kuota_terampil_awal","kuota_terampil_terdaftar"],context):
                kuota = record['kuota_terampil_awal'] - record['kuota_terampil_terdaftar']
                res[record['id']]=kuota
        return res
    _columns = {
        'name'          : fields.char('Formasi',size=328),
        'jabatan_id': fields.many2one('jabatan.fungsional.tertentu', 'Jabatan' ),
        'kuota_ahli'         : fields.function(_get_kuota_ahli, method=True,readonly=True , store=False,
                                         type="integer",string='Kuota Ahli'),
        'kuota_terampil'         : fields.function(_get_kuota_terampil, method=True,readonly=True , store=False,
                                         type="integer",string='Kuota Terampil'),
        'kuota_ahli_awal'         : fields.integer('Kuota Ahli (Awal)'),
        'kuota_terampil_awal'         : fields.integer('Kuota Terampil (Awal)'),
        'kuota_ahli_terdaftar'         : fields.integer('Kuota Ahli (Terdaftar)'),
        'kuota_terampil_terdaftar'         : fields.integer('Kuota Terampil (Terdaftar)'),
        
        'tanggal_penetapan'     : fields.date('Tanggal Penetapan'),
        'company_id'    : fields.many2one('res.company', 'OPD' ,required=True ,domain="[('parent_id','!=',1)]"),
        'instansi_pembina_id': fields.many2one('instansi.pembina', 'Instansi Pembina' ),
        'active'        : fields.boolean('Aktif'),
        
        #### untuk syarat
        'usia_minimum'  : fields.integer('Minmum Usia'),
        #'golongan_ids': fields.one2many('golongan.jabatan.fungsional.tertentu','formasi_id', 'Minimum Golongan'),
        #'study_degree_ids': fields.one2many('study.degree.jabatan.fungsional.tertentu','formasi_id', 'Minimum Pendidikan'),
        
        
        
    }
    _defaults = {
        'active' : True,
    }
   
    def count_kuota(self, cr, uid, ids,mode,code, context=None):

        for obj in self.browse(cr,uid,ids,context=None) :
            kuota =0;
            if code == 'ahli':
                kuota=obj.kuota_ahli_terdaftar;
                if mode == 'plus' :
                    kuota=obj.kuota_ahli_terdaftar+1
                elif mode == 'min' :
                    kuota=obj.kuota_ahli_terdaftar-1
                self.write(cr, uid, [obj.id], {'kuota_ahli_terdaftar':kuota}, context=context) 
            if code == 'terampil':
                kuota=obj.kuota_terampil_terdaftar;
                if mode == 'plus' :
                    kuota=obj.kuota_terampil_terdaftar+1
                elif mode == 'min' :
                    kuota=obj.kuota_terampil_terdaftar-1
                self.write(cr, uid, [obj.id], {'kuota_terampil_terdaftar':kuota}, context=context)
        
        return kuota
formasi_jabatan_fungsional_tertentu()



