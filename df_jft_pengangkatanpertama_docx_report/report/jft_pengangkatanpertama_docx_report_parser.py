import time
from openerp.report import report_sxw
#from openerp.addons.df_jft_pengangkatanpertama.konfigurasi_periode_jft_pengangkatanpertama import PERIODE_BULAN_LIST
import locale

class jft_pengangkatanpertama_docx_report_parser(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context=None):
        locale.setlocale(locale.LC_ALL, 'id_ID.utf8')
        super(jft_pengangkatanpertama_docx_report_parser, self).__init__(cr, uid, name, context=context)

    def get_jft_pengangkatanpertama_docx_report_raw(self,filters,context=None):

        domain = []

        layanan_pool = self.pool.get('layanan.pengangkatan.pertama')
        if filters['form']['layanan_id']:
            layanan_id = filters['form']['layanan_id'][0]
            domain.append(('id','=',layanan_id))


        result_ids=layanan_pool.search(self.cr, self.uid,domain , context=None)
        results = layanan_pool.browse(self.cr, self.uid, result_ids)

        return results

    def get_subheader_layanan(self,d,upper=False):
        lbl = self.get_jabatan_lbl(d)+ ' '+self.get_jenjang_jbt_lbl(d) + ' ATAS NAMA ' + d.employee_id.fullname
        if upper:
            return lbl.upper()
        return lbl
    def get_formasi_lbl(self,d,upper=True):
        lbl = d and d.formasi_id and d.formasi_id.name or ''
        if upper:
            return lbl.upper()
        return lbl
    def get_jabatan_lbl(self,d,upper=True):
        lbl = d and d.jabatan_id and d.jabatan_id.name or ''
        if upper:
            return lbl.upper()
        return lbl
    def get_jenjang_jbt_lbl(self,d,upper=True):
        lbl = d and d.jenjang_jabatan_id and d.jenjang_jabatan_id.name or ''
        if upper:
            return lbl.upper()
        return lbl

    def get_nama_pegawai(self,d,upper=False):
        lbl = d and d.employee_id and d.employee_id.fullname or ''
        if upper:
            return lbl.upper()
        return lbl
    def get_nip_pegawai(self,d,upper=False):
        lbl = d and d.employee_id and d.employee_id.nip or ''
        if upper:
            return lbl.upper()
        return lbl
    def get_jabatan_pegawai(self,d,upper=False):
        lbl = d and d.employee_id and d.job_id and d.job_id.name or ''
        if upper:
            return lbl.upper()
        return lbl
    def get_golongan_pegawai(self,d,upper=False):
        lbl = d and d.employee_id and d.golongan_id and d.golongan_id.description or ''
        if upper:
            return lbl.upper()
        return lbl
    def get_opd_pegawai(self,d,upper=False):
        lbl = d and d.company_id and d.company_id.name or ''
        if upper:
            return lbl.upper()
        return lbl

    def get_ak_words(self,d,content):
        words = content
        jbt_baru_lbl = self.get_jabatan_lbl(d,False)
        jenjang_jbt_lbl = self.get_jenjang_jbt_lbl(d,False)
        nilai_pak = d.nilai_pak or 0
        words= words.replace('[P_JABATAN_BARU]',jbt_baru_lbl)
        words= words.replace('[P_JENJANG_JABATAN]',jenjang_jbt_lbl)
        words= words.replace('[P_AK]',str(nilai_pak))
        return words
    def get_tunjangan_words(self,d,content):
        words = content
        tunjangan = d.tunjangan_jabatan or 0
        str_tunjangan = str(tunjangan)
        try :
            str_tunjangan = str(tunjangan).split(".")[0]
        except:
            str_tunjangan = str(tunjangan)
        tunjangan_terbilang = self._get_terbilang(tunjangan) + " Rupiah"
        words= words.replace('[P_TUNJANGAN]',str_tunjangan)
        words= words.replace('[P_TUNJANGAN_TERBILANG]',tunjangan_terbilang)

        return words
    def get_nodin_par_words(self,d,content):
        words = content


        jbt_baru_lbl = self.get_jabatan_lbl(d,False)
        jenjang_jbt_lbl = self.get_jenjang_jbt_lbl(d,False)
        nama_lbl = self.get_nama_pegawai(d)

        words= words.replace('[P_NODIN_JABATAN_BARU]',jbt_baru_lbl)
        words= words.replace('[P_NODIN_JENJANG_JABATAN_BARU]',jenjang_jbt_lbl)
        words= words.replace('[P_NODIN_NAMA]',nama_lbl)
        return words
    def get_nodin_pengantar_words(self,d,content):
        words = content
        opd_lbl = self.get_opd_pegawai(d,False)
        jbt_baru_lbl = self.get_jabatan_lbl(d,False)
        jenjang_jbt_lbl = self.get_jenjang_jbt_lbl(d,False)
        nama_lbl = self.get_nama_pegawai(d)
        pengantar_lbl = (d.no_surat_pengantar or '-') +' '+'Tanggal'+' '+self._get_format_date(d.tanggal_surat_pengantar)

        words= words.replace('[P_NODIN_OPD]',opd_lbl)
        words= words.replace('[P_NODIN_NO_SURAT_PENGANTAR_TANGGAL]',pengantar_lbl)
        words= words.replace('[P_NODIN_JABATAN_BARU]',jbt_baru_lbl)
        words= words.replace('[P_NODIN_JENJANG_JABATAN_BARU]',jenjang_jbt_lbl)
        words= words.replace('[P_NODIN_NAMA]',nama_lbl)
        return words

    def _get_terbilang(self,x):
        satuan=["","Satu","Dua","Tiga","Empat","Lima","Enam","Tujuh","Delapan","Sembilan","Sepuluh","Sebelas"]
        n = int(x)
        if n >= 0 and n <= 11:
            Hasil = " " + satuan[n]
        elif n >= 12 and n <= 19:
            Hasil = self._get_terbilang(n % 10) + " Belas"
        elif n >= 20 and n <= 99:
            Hasil = self._get_terbilang(n / 10) + " Puluh" + self._get_terbilang(n % 10)
        elif n >= 100 and n <= 199:
            Hasil = " Seratus" + self._get_terbilang(n - 100)
        elif n >= 200 and n <= 999:
            Hasil = self._get_terbilang(n / 100) + " Ratus" + self._get_terbilang(n % 100)
        elif n >= 1000 and n <= 1999:
            Hasil = " Seribu" + self._get_terbilang(n - 1000)
        elif n >= 2000 and n <= 999999:
            Hasil = self._get_terbilang(n / 1000) + " Ribu" + self._get_terbilang(n % 1000)
        elif n >= 1000000 and n <= 999999999:
            Hasil = self._get_terbilang(n / 1000000) + " Juta" + self._get_terbilang(n % 1000000)
        else:
            Hasil = self._get_terbilang(n / 1000000000) + " Milyar" + self._get_terbilang(n % 100000000)

        return Hasil
    def _get_format_date(self,a_date):
        formatted_print_date=''
        try :
            formatted_print_date = time.strftime('%d %B %Y', time.strptime(a_date,'%Y-%m-%d'))
        except :
            formatted_print_date='-'

        return formatted_print_date

    '''
    def get_period_title(self,prefix,filters):

        title='-';

        if filters['form']['periode_bulan'] and filters['form']['periode_tahun']:
            title = self.get_period_month_label(filters['form']['periode_bulan'])+ " - " + filters['form']['periode_tahun']
        return prefix+title
    def get_company_title(self,prefix,filters):

        title='-';

        if filters['form']['company_id']:
            title = filters['form']['company_id'][1]
        return prefix+title
    def get_period_month_label(self,val):

        label=val
        for data in PERIODE_BULAN_LIST :
            if data[0] == val:
                return data[1]

        return label
    def get_state_label(self,val):

        label=val
        for data in PENDAFTARAN_UJIAN_STATE :
            if data[0] == val:
                return data[1]

        return label
        '''
   
    
