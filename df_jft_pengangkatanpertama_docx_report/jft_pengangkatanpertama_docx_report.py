from openerp.osv import fields, osv
import time


class layanan_pengangkatan_pertama(osv.Model):
    _inherit = 'layanan.pengangkatan.pertama'
    #Laporan
    def download_kepgub_pengangkatanpertama_individu(self, cr, uid, ids, context={}):
        obj = self.browse(cr,uid,ids,context)[0]
        value = {'layanan_id': [obj.id, obj.name], 'id': obj.id}

        datas = {
            'ids': context.get('active_ids',[]),
            'model': 'jft.pengangkatanpertama.docx.report',
            'form': value
        }

        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'jft.pengangkatanpertama.docx.xls',
            'report_type': 'webkit',
            'datas': datas,
        }

layanan_pengangkatan_pertama()

class jft_pengangkatanpertama_docx_report(osv.osv_memory):
    
    _name = "jft.pengangkatanpertama.docx.report"
    _columns = {
        'layanan_id'        : fields.many2one('layanan.pengangkatan.pertama', 'Pengangkatan Pertama'),

    }
    _defaults = {
        'layanan_id':3
    }

    
    def get_jft_pengangkatanpertama_docx_report(self, cr, uid, ids, context={}):
        value = self.read(cr, uid, ids)[0]
        print " value ",value
        datas = {
            'ids': context.get('active_ids',[]),
            'model': 'jft.pengangkatanpertama.docx.report',
            'form': value
        }
	
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'jft.pengangkatanpertama.docx.xls',
            'report_type': 'webkit',
            'datas': datas,
        }
    
jft_pengangkatanpertama_docx_report()
