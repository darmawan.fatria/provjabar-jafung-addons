from openerp.osv import fields, osv
from string import split

class app_jafung_settings(osv.osv_memory):
    _name = 'app.jafung.settings'
    _description = 'Konfigurasi Aplikasi Jafung'

    _columns = {
            'name' : fields.char('Konfigurasi Integrasi SKP SIPKD', size=64, readonly=True),
           
    }
    def action_release_all_expired_services(self, cr, uid, ids, context={}):

        validasi_layanan_pool = self.pool.get('validasi.layanan.jft')
        validasi_layanan_pool.release_expire_all_layanan(cr,uid,[],context=None)
        return {}
   
app_jafung_settings()