##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.asdarfat.wordpress.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

{
    "name": "Keputusan Gubernur Alih Tingkat",
    "version": "1.0",
    "author": "darmawan fatriananda",
    "category": "Jafung /Reporting / Alih Tingkat",
    "description": """
  Alih Tingkat
    """,
    "website" : "http://www.mediasee.net",
    "license" : "GPL-3",
    "depends": [
                "df_jabatan_fungsional_tertentu",
                ],
    'data': [
                   "alih_tingkat_report_view.xml",
                   "report/alih_tingkat_individu_pdf_report_view.xml",
                   "report/alih_tingkat_nominatif_pdf_report_view.xml",
                   "report/alih_tingkat_lampiran_nominatif_pdf_report_view.xml",
                   "report/alih_tingkat_petikan_nominatif_pdf_report_view.xml",
                   "report/alih_tingkat_nodin_nominatif_pdf_report_view.xml",
                   ],
    'installable': True,
    'active': False,

}