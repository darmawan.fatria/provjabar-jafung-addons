from docx import Document
from docx.shared import Pt
from docx.enum.text import WD_BREAK
import cStringIO
import StringIO
from report_engine_xls import report_xls
from jft_kenaikanjabatan_docx_report_parser import jft_kenaikanjabatan_docx_report_parser

class jft_kenaikanjabatan_docx_report_doc_generator(report_xls):

    def generate_doc_report(self, parser, filters, obj, document):

        print "add title....";
        layanan_header = 'KENAIKAN DALAM JABATAN FUNGSIONAL'
        jabatan_ak_content='''dalam jabatan fungsional [P_JABATAN_BARU] [P_JENJANG_JABATAN] dengan Angka Kredit sebesar [P_AK] .'''
        jabatan_tunjangan_content='''Kepada Pegawai Negeri Sipil sebagaimana tersebut pada Diktum KESATU, diberikan tunjangan jabatan fungsional sebesar         Rp. [P_TUNJANGAN],- ([P_TUNJANGAN_TERBILANG]) setiap bulannya.'''
        nodin_par_content='''Bersama ini kami sampaikan naskah Keputusan Gubernur Jawa Barat tentang Pengangkatan dalam Jabatan fungsional [P_NODIN_JABATAN_BARU] [P_NODIN_JENJANG_JABATAN_BARU] atas nama [P_NODIN_NAMA]. Perlu kami informasikan:'''
        nodin_surat_pengantar='''berdasarkan Surat Kepala [P_NODIN_OPD] Nomor [P_NODIN_NO_SURAT_PENGANTAR_TANGGAL], Sdr/i. [P_NODIN_NAMA]. dipandang cakap dan memenuhi persyaratan yang diperlukan untuk diangkat dalam jabatan fungsional [P_NODIN_JABATAN_BARU] [P_NODIN_JENJANG_JABATAN_BARU].'''
        datas = parser.get_jft_kenaikanjabatan_docx_report_raw(filters)
        for data in datas:
            for paragraph in document.paragraphs:
                if '[P_LAYANAN]' in paragraph.text:
                    paragraph.text = layanan_header;
                if '[P_JABATAN_TINGKAT_NAMA]' in paragraph.text:
                    paragraph.text = parser.get_subheader_layanan(data)
                if '[P_NODIN_JABATAN_BARU]' in paragraph.text:
                    paragraph.text = parser.get_nodin_par_words(data,nodin_par_content)
                if '[P_NODIN_NO_SURAT_PENGANTAR_TANGGAL]' in paragraph.text:
                    paragraph.text =parser.get_nodin_pengantar_words(data,nodin_surat_pengantar)

            for table in document.tables:
                for row in table.rows:
                    for cell in row.cells:
                        if '[P_TUNJANGAN_DAN_TERBILANG]' in cell.text:
                            cell.text = parser.get_tunjangan_words(data,jabatan_tunjangan_content)




                        for cell_table in cell.tables:
                            for cell_row in cell_table.rows:
                                for cell_cell in cell_row.cells:

                                    if '[P_NAMA]' in cell_cell.text:
                                        cell_cell.text = parser.get_nama_pegawai(data)
                                    if '[P_NIP]' in cell_cell.text:
                                        cell_cell.text = parser.get_nip_pegawai(data)
                                    if '[P_JABATAN_LAMA]' in cell_cell.text:
                                        cell_cell.text = parser.get_jabatan_pegawai(data)
                                    if '[P_PANGKAT_GOLONGAN]' in cell_cell.text:
                                        cell_cell.text = parser.get_golongan_pegawai(data)
                                    if '[P_OPD]' in cell_cell.text:
                                        cell_cell.text = parser.get_opd_pegawai(data)

                                    if '[P_NILAI_AK_DAN_TERBILANG]' in cell_cell.text:
                                        cell_cell.text = parser.get_ak_words(data,jabatan_ak_content)


        #document.add_heading(title, 0)


    # Override from report_engine_xls.py	
    def create_source_xls(self, cr, uid, ids, filters, report_xml, context=None): 
        if not context: context = {}
	
	# Avoiding context's values change
        context_clone = context.copy()
	
        rml_parser = self.parser(cr, uid, self.name2, context=context_clone)
        objects = self.getObjects(cr, uid, ids, context=context_clone)
        rml_parser.set_context(objects, filters, ids, 'xls')
        io = cStringIO.StringIO()
        #document = Document('/home/darfat/Downloads/template_kenaikan_jabatan_3.docx')


        fpath='/home/darfat/Odoo/provjabar-jafung-addons/df_jft_kenaikanjabatan_docx_report/report/'
        fname='jafung_template_kenaikanjabatan.docx';
        f = open(fpath+''+fname)
        document = Document(f)
        style = document.styles['Normal']
        font = style.font
        font.name = 'Bookman Old Style'
        font.size = Pt(12)
        self.generate_doc_report(rml_parser, filters, rml_parser.localcontext['objects'], document)
        f.close()
        document.save(io)
        io.seek(0)
        return (io.read(), 'docx')

#Start the reporting service
jft_kenaikanjabatan_docx_report_doc_generator(
    #name (will be referred from jft_kenaikanjabatan_docx_report.py, must add "report." as prefix)
    'report.jft.kenaikanjabatan.docx.xls',
    #model
    'jft.kenaikanjabatan.docx.report',
    #file
    'addons/df_jft_kenaikanjabatan_docx_report/report/jft_kenaikanjabatan_docx_report.xls',
    #parser
    parser=jft_kenaikanjabatan_docx_report_parser,
    #header
    header=True
)