# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.mediasee.net>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

##############################################################################
# Release Notes
# 2015-03-014 : 1. Init  
#  
##############################################################################
from openerp.osv import fields, osv
from openerp.tools.translate import _
from datetime import datetime,timedelta
import time



class layanan_pengangkatan_pertama(osv.Model):
    _inherit = 'layanan.pengangkatan.pertama'
    #Laporan
    def download_kepgub_pengangkatan_individu(self, cr, uid, ids, context={}):
        if context is None:
            context = {}
        datas = {'data_ids':ids,
                 }
        return self.pool['report'].get_action(cr, uid, ids, 
                        'df_jft_pengangkatan_pdf_report.report_pengangkatan_pertama_individu_report', 
                        data=datas, context=context)
        
    def action_create_download_kepgub_pengangkatan_nominatif_popup(self, cr, uid, ids, context=None):
        """ Pengajuan Pengangkatan Pertama Ahli
        """
        
        if not ids: return []
        dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'df_jft_pengangkatan_pdf_report', 'kepgub_pengangkatan_pertama_nominatif_form_view')
        layanan_pengangkatan_pertama = self.browse(cr, uid, ids[0], context=context)
        
        #validasi
        if not layanan_pengangkatan_pertama :
             raise osv.except_osv(_('Proses Pengajuan Tidak Bisa Dilanjutkan'),
                                _('Silahkan Lengkapi Data Kepegawaian Anda, Atau Hubungi Admin'))
        
        return {
                'name':_("Laporan Nominatif Pengangkatan Pertama"),
                'view_mode': 'form',
                'view_id': view_id,
                'view_type': 'form',
                'res_model': 'layanan.pengangkatan.pertama.nominatif.param',
                'type': 'ir.actions.act_window',
                'nodestroy': True,
                'target': 'new',
                'domain': '[]',
                'context': {
                    'default_formasi_id':layanan_pengangkatan_pertama.formasi_id.id,
                    'default_company_id':layanan_pengangkatan_pertama.company_id.id,
                    #'default_employee_count':0,
                  
                }
            }
layanan_pengangkatan_pertama()
class layanan_pengangkatan_pertama_nominatif_param(osv.osv_memory):
    _name ='layanan.pengangkatan.pertama.nominatif.param'
    _description = 'Pengangkatan Pertama Nominatif Parameter Report'
    def on_change_formasi(self, cr, uid, ids, company_id,formasi_id, context=None):
        res = {'value': {'employee_count': 0}}
        if formasi_id:
            layanan_pool = self.pool.get('layanan.pengangkatan.pertama')
            layanan_data_ids = layanan_pool.search(cr,uid,[('formasi_id','=',formasi_id),
                                                                     ('state','=','confirm')],context=None)
            if layanan_data_ids:
                res['value']['employee_count'] = len(layanan_data_ids)
        return res
    
    def _counting_data(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        
        count=0
        for data in self.browse(cr,uid,ids,context=None):
            count=0
            formasi_id = data.formasi_id.id
            layanan_data_ids = layanan_pool.search(self.cr,self.uid,[('formasi_id','=',formasi_id),
                                                                     ('state','=','confirm')],context=None)
            if layanan_data_ids:
                count = len(layanan_data_ids)
            res[data.id]=count
        return res
    _columns = {
        'name'       : fields.char('Nama', size=4),
        'company_id'                : fields.many2one('res.company', 'OPD' ,required=True ),
        'formasi_id'                : fields.many2one('formasi.jabatan.fungsional.tertentu', 'Formasi' ,required=True),
        'employee_count': fields.integer('Jumlah Pengajuan'),
    }
    #Laporan
    def download_kepgub_pengangkatan_nominatif(self, cr, uid, ids, context={}):
        if context is None:
            context = {}
        value = self.read(cr, uid, ids)[0]
        if value:
            #if value['employee_count'] < 1 :
            #    raise osv.except_osv(_('Laporan Tidak Bisa Di Download'),
            #                            _('Jumlah Pengajuan Minimal 2 Pegawai.'))
            datas = {'data_ids':ids,
                     'form': value
                     }
            
        return self.pool['report'].get_action(cr, uid, ids, 
                        'df_jft_pengangkatan_pdf_report.report_pengangkatan_pertama_nominatif_report', 
                        data=datas, context=context)
    def download_lampiran_pengangkatan_nominatif(self, cr, uid, ids, context={}):
        if context is None:
            context = {}
        value = self.read(cr, uid, ids)[0]
        if value:
            #if value['employee_count'] < 1 :
            #    raise osv.except_osv(_('Laporan Tidak Bisa Di Download'),
            #                            _('Jumlah Pengajuan Minimal 2 Pegawai.'))
            datas = {'data_ids':ids,
                     'form': value
                     }
            
        return self.pool['report'].get_action(cr, uid, ids, 
                        'df_jft_pengangkatan_pdf_report.report_pengangkatan_pertama_lampiran_nominatif_report', 
                        data=datas, context=context)
    def download_petikan_pengangkatan_nominatif(self, cr, uid, ids, context={}):
        if context is None:
            context = {}
        value = self.read(cr, uid, ids)[0]
        if value:
            #if value['employee_count'] < 1 :
            #    raise osv.except_osv(_('Laporan Tidak Bisa Di Download'),
            #                            _('Jumlah Pengajuan Minimal 2 Pegawai.'))
            datas = {'data_ids':ids,
                     'form': value
                     }
            
        return self.pool['report'].get_action(cr, uid, ids, 
                        'df_jft_pengangkatan_pdf_report.report_pengangkatan_pertama_petikan_nominatif_report', 
                        data=datas, context=context)
    def download_nodin_pengangkatan_nominatif(self, cr, uid, ids, context={}):
        if context is None:
            context = {}
        value = self.read(cr, uid, ids)[0]
        if value:
            #if value['employee_count'] < 1 :
            #    raise osv.except_osv(_('Laporan Tidak Bisa Di Download'),
            #                            _('Jumlah Pengajuan Minimal 2 Pegawai.'))
            datas = {'data_ids':ids,
                     'form': value
                     }
            
        return self.pool['report'].get_action(cr, uid, ids, 
                        'df_jft_pengangkatan_pdf_report.report_pengangkatan_pertama_nodin_nominatif_report', 
                        data=datas, context=context)
layanan_pengangkatan_pertama_nominatif_param() 
    