from datetime import datetime,timedelta
import time
from openerp.osv import osv
from openerp.report import report_sxw
import locale
from string import upper


class report_pengangkatan_kembali_report(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context=None):
        locale.setlocale(locale.LC_ALL, 'id_ID.utf8')
        super(report_pengangkatan_kembali_report, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'get_pengangkatan_kembali_individu' : self._get_pengangkatan_kembali_individu,
            'get_pengangkatan_kembali_nominatif' : self._get_pengangkatan_kembali_nominatif,
            'get_single_pengangkatan_kembali_nominatif' : self._get_single_pengangkatan_kembali_nominatif,
            'get_format_date' : self._get_format_date,
            'get_terbilang' : self._get_terbilang,
            'get_param_by_key':self._get_param_by_key,
            'to_upper':self._to_upper,
           
        })
    def _get_pengangkatan_kembali_individu(self,data):
        obj_data=self.pool.get('layanan.pengangkatan.kembali').browse(self.cr,self.uid,data['data_ids'])
        return obj_data
    def _get_pengangkatan_kembali_nominatif(self,data):
        formasi_pool = self.pool.get('formasi.jabatan.fungsional.tertentu')
        layanan_pool = self.pool.get('layanan.pengangkatan.kembali')
        if data['form'] :
            company_id = data['form']['company_id'][0]
            formasi_id = data['form']['formasi_id'][0]
            
            layanan_data_ids = layanan_pool.search(self.cr,self.uid,[('formasi_id','=',formasi_id),
                                                                     ('state','=','confirm')],context=None)
        obj_data=layanan_pool.browse(self.cr,self.uid,layanan_data_ids,context=None)
        return obj_data
    def _get_single_pengangkatan_kembali_nominatif(self,data):
        formasi_pool = self.pool.get('formasi.jabatan.fungsional.tertentu')
        layanan_pool = self.pool.get('layanan.pengangkatan.kembali')
        if data['form'] :
            company_id = data['form']['company_id'][0]
            formasi_id = data['form']['formasi_id'][0]
            
            layanan_data_ids = layanan_pool.search(self.cr,self.uid,[('formasi_id','=',formasi_id),
                                                                     ('state','=','confirm')],context=None)
        obj_data=layanan_pool.browse(self.cr,self.uid,layanan_data_ids[0],context=None)
        return obj_data
    def _get_param_by_key(self,key):
        single_value=self.pool.get('global.parameter.jft').get_value_from_key(self.cr,self.uid,key)
        return single_value;
    def _get_format_date(self,a_date):
        formatted_print_date=''
        try :
            formatted_print_date = time.strftime('%d %B %Y', time.strptime(a_date,'%Y-%m-%d'))
        except :
            formatted_print_date='-'
            
        return formatted_print_date
    
    def _to_upper(self,a_string):
        return a_string and upper(a_string) or ''
    
    def _get_terbilang(self,x):
        satuan=["","Satu","Dua","Tiga","Empat","Lima","Enam","Tujuh","Delapan","Sembilan","Sepuluh","Sebelas"]
        n = int(x)
        if n >= 0 and n <= 11:
            Hasil = " " + satuan[n]
        elif n >= 12 and n <= 19:
            Hasil = self._get_terbilang(n % 10) + " Belas"
        elif n >= 20 and n <= 99:
            Hasil = self._get_terbilang(n / 10) + " Puluh" + self._get_terbilang(n % 10)
        elif n >= 100 and n <= 199:
            Hasil = " Seratus" + self._get_terbilang(n - 100)
        elif n >= 200 and n <= 999:
            Hasil = self._get_terbilang(n / 100) + " Ratus" + self._get_terbilang(n % 100)
        elif n >= 1000 and n <= 1999:
            Hasil = " Seribu" + self._get_terbilang(n - 1000)
        elif n >= 2000 and n <= 999999:
            Hasil = self._get_terbilang(n / 1000) + " Ribu" + self._get_terbilang(n % 1000)
        elif n >= 1000000 and n <= 999999999:
            Hasil = self._get_terbilang(n / 1000000) + " Juta" + self._get_terbilang(n % 1000000)
        else:
            Hasil = self._get_terbilang(n / 1000000000) + " Milyar" + self._get_terbilang(n % 100000000)
        
        return Hasil
class wrapped_report_pengangkatan_kembali_individu(osv.AbstractModel):
    _name = 'report.df_jft_pengangkatan_kembali_pdf_report.report_pengangkatan_kembali_individu_report'
    _inherit = 'report.abstract_report'
    _template = 'df_jft_pengangkatan_kembali_pdf_report.report_pengangkatan_kembali_individu_report'
    _wrapped_report_class = report_pengangkatan_kembali_report
class wrapped_report_pengangkatan_kembali_nominatif(osv.AbstractModel):
    _name = 'report.df_jft_pengangkatan_kembali_pdf_report.report_pengangkatan_kembali_nominatif_report'
    _inherit = 'report.abstract_report'
    _template = 'df_jft_pengangkatan_kembali_pdf_report.report_pengangkatan_kembali_nominatif_report'
    _wrapped_report_class = report_pengangkatan_kembali_report
class wrapped_report_pengangkatan_kembali_lampiran_nominatif(osv.AbstractModel):
    _name = 'report.df_jft_pengangkatan_kembali_pdf_report.report_pengangkatan_kembali_lampiran_nominatif_report'
    _inherit = 'report.abstract_report'
    _template = 'df_jft_pengangkatan_kembali_pdf_report.report_pengangkatan_kembali_lampiran_nominatif_report'
    _wrapped_report_class = report_pengangkatan_kembali_report
class wrapped_report_pengangkatan_kembali_petikan_nominatif(osv.AbstractModel):
    _name = 'report.df_jft_pengangkatan_kembali_pdf_report.report_pengangkatan_kembali_petikan_nominatif_report'
    _inherit = 'report.abstract_report'
    _template = 'df_jft_pengangkatan_kembali_pdf_report.report_pengangkatan_kembali_petikan_nominatif_report'
    _wrapped_report_class = report_pengangkatan_kembali_report
class wrapped_report_pengangkatan_kembali_nodin_nominatif(osv.AbstractModel):
    _name = 'report.df_jft_pengangkatan_kembali_pdf_report.report_pengangkatan_kembali_nodin_nominatif_report'
    _inherit = 'report.abstract_report'
    _template = 'df_jft_pengangkatan_kembali_pdf_report.report_pengangkatan_kembali_nodin_nominatif_report'
    _wrapped_report_class = report_pengangkatan_kembali_report