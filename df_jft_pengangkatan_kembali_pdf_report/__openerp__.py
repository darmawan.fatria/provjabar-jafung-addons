##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.asdarfat.wordpress.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

{
    "name": "Keputusan Gubernur Pengangkatan Kembali",
    "version": "1.0",
    "author": "darmawan fatriananda",
    "category": "Jafung /Reporting / Pengangkatan Kembali",
    "description": """
  Pengangkatan Kembali
    """,
    "website" : "http://www.mediasee.net",
    "license" : "GPL-3",
    "depends": [
                "df_jabatan_fungsional_tertentu",
                ],
    'data': [
                   "pengangkatan_kembali_report_view.xml",
                   "report/pengangkatan_kembali_individu_pdf_report_view.xml",
                   "report/pengangkatan_kembali_nominatif_pdf_report_view.xml",
                   "report/pengangkatan_kembali_lampiran_nominatif_pdf_report_view.xml",
                   "report/pengangkatan_kembali_petikan_nominatif_pdf_report_view.xml",
                   "report/pengangkatan_kembali_nodin_nominatif_pdf_report_view.xml",
                   ],
    'installable': True,
    'active': False,

}