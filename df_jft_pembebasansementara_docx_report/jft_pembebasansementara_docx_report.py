from openerp.osv import fields, osv
import time


class layanan_pembebasan_sementara(osv.Model):
    _inherit = 'layanan.pembebasan.sementara'
    #Laporan
    def download_kepgub_pembebasansementara_individu(self, cr, uid, ids, context={}):
        obj = self.browse(cr,uid,ids,context)[0]
        value = {'layanan_id': [obj.id, obj.name], 'id': obj.id}

        datas = {
            'ids': context.get('active_ids',[]),
            'model': 'jft.pembebasansementara.docx.report',
            'form': value
        }

        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'jft.pembebasansementara.docx.xls',
            'report_type': 'webkit',
            'datas': datas,
        }

layanan_pembebasan_sementara()

class jft_pembebasansementara_docx_report(osv.osv_memory):
    
    _name = "jft.pembebasansementara.docx.report"
    _columns = {
        'layanan_id'        : fields.many2one('layanan.pembebasan.sementara', 'Pembebasan Sementara'),

    }
    _defaults = {
        'layanan_id':3
    }

    
    def get_jft_pembebasansementara_docx_report(self, cr, uid, ids, context={}):
        value = self.read(cr, uid, ids)[0]
        print " value ",value
        datas = {
            'ids': context.get('active_ids',[]),
            'model': 'jft.pembebasansementara.docx.report',
            'form': value
        }
	
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'jft.pembebasansementara.docx.xls',
            'report_type': 'webkit',
            'datas': datas,
        }
    
jft_pembebasansementara_docx_report()
